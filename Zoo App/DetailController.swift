//
//  DetailController.swift
//  Zoo App
//
//  Created by Muhamad Widi Aryanto on 05/09/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit

class DetailController: UIViewController {
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var titleDetail: UILabel!
    @IBOutlet weak var subtitleDetail: UILabel!
    
    var detailImage:UIImage?
    var detailTitle:String?
    var detailSubtitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageDetail.image = detailImage
        titleDetail.text = detailTitle
        subtitleDetail.text = detailSubtitle
        // Do any additional setup after loading the view.
        self.navigationItem.title = detailTitle
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
