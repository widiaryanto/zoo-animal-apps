//
//  ViewController.swift
//  Zoo App
//
//  Created by Muhamad Widi Aryanto on 04/09/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var arrayTitle = [
        "Kucing",
        "Ayam",
        "Sapi",
        "Anjing",
        "Gajah",
        "Kambing",
        "Kuda",
        "Singa",
        "Babi",
        "Domba"
    ]
    var arraySubtitle = [
        "Ilmiah: Felis Silves Tris/Catus",
        "Ilmiah: Gallus Gallus Domesticus",
        "Ilmiah: Bos Taurus",
        "Ilmiah: Canis Lupus Familiaris/Dingo",
        "Ilmiah: Loxodonta/Elephas",
        "Ilmiah: Capra Aegagrus Hircus",
        "Ilmiah: Equus Caballus",
        "Ilmiah: Panthera Leo",
        "Ilmiah: Sus Scrofa Domesticus",
        "Ilmiah: Ovis Aries"
    ]
    var arrayImage = [
        UIImage(named: "cat")!,
        UIImage(named: "chicken")!,
        UIImage(named: "cow")!,
        UIImage(named: "dog")!,
        UIImage(named: "elephant")!,
        UIImage(named: "goat")!,
        UIImage(named: "horse")!,
        UIImage(named: "lion")!,
        UIImage(named: "pig")!,
        UIImage(named: "sheep")!
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        self.navigationItem.title = "Zoo Apps"
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.titleCell.text = arrayTitle[indexPath.row]
        cell.subtitleCell.text = arraySubtitle[indexPath.row]
        cell.imageCell.image = arrayImage[indexPath.row]
        
        // cell.imageCell.layer.cornerRadius = cell.imageCell.frame.height / 2
        // cell.imageCell.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "detailController") as! DetailController
        
        controller.detailImage = arrayImage[indexPath.row]
        controller.detailTitle = arrayTitle[indexPath.row]
        controller.detailSubtitle = arraySubtitle[indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}
